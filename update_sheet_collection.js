const fs = require('fs');

const path = 'public/sheet';

function generateIndex(files)
{
    let data = new Uint8Array(Buffer.from(JSON.stringify({data:files})));
    fs.writeFile('public/data/sheet_collection.json', data, (err) => {
        if (err) throw err;
        console.log('索引完成更新！');
    });
}

fs.readdir(path, null, function (err, files){
    if (err)
    {
        console.log(err);
        return;
    }

    generateIndex(files)
});



