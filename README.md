# WebAccompanySheet

---

### 介绍
Web 伴奏谱

### 软件架构
纯 Web 客户端（HTML+CSS+JavaScript）实现，视图使用 Vue 渲染，乐谱使用开源的 JSON 格式表示。

### 浏览器要求：

* iOS Safari 3.2+
* Chrome for Android 75+
* Firefox for Android 67+
* Firefox 3.5+
* Chrome 4+
* Internet Explorer 8+
* Edge

---

### 使用说明

打开 <a href="http://oschina_xiaowen.gitee.io/webaccompanysheet/index.html" target="_blank">index.html</a> ，选择乐谱，查看。

### 目录结构：

* data  数据
* sheet  乐谱(JSON格式)

### 乐谱格式：

* title  (String)  标题
* desc   (String)  描述（在标题下面）
* beat   (Number)  拍子
* rhythm (String)  节拍（4/4,3/4,2/2,6/8 之类）
* tempo  (Number)  速度，每分钟
* data   (Array)   乐谱主要数据，每一个元素是一个小节的数据
*
 * section  (String)  必要，和弦标记，用空格分隔
 * tag      (String)  可选，小节标记，可以标记分部、声量等
* note   (String)  其它说明，可以介绍一下作品的风格、曲式，或者演奏技巧

---

### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
