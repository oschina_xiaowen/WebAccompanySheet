/**
 * Created by Kevin on 2019/8/14.
 */

var pageConfig = {
    apiConfig : {
        list : {
            url : 'data/sheet_collection.json'
        }
    }
};

var sheetIndexContent = null;
function initView()
{
    sheetIndexContent = initVueList({
        el : '#sheetIndexContent',
        apiConfig : pageConfig.apiConfig,
        afterLoadData : function (data){
            sheetIndexContent.list = data;
        },
        data : {
            filterList : []
        },
        methods : {
            onQuery : function (){
                // remove old data
                this.filterList = [];
                if (this.keywords.length < 1)
                {
                    return;
                }

                for (var i=0; i<this.list.length; i++)
                {
                    if (this.list[i].indexOf(this.keywords) >= 0)
                    {
                        this.filterList.push(this.list[i]);
                    }
                }
            },
            getSimpleFileName : function (fileName){
                var fileNameParts = fileName.split('.');
                if (fileNameParts.length > 1)
                {
                    fileNameParts.pop();
                }

                fileName = fileNameParts.join('.');

                return fileName;
            },
            onClickItem : function (event){
                var target = event.currentTarget;
                var file = target.dataset.file || '';
                if (!file)
                {
                    alert('检查文件失败');
                    return;
                }

                location.href = 'music_sheet.html?sheetId=' + file;
            }
        }
    });
}

// check session storage
if (typeof(localStorage) === 'undefined')
{
    alert('当前浏览器版本太旧，请更新浏览器。');
}
else
{
    initView()
}

