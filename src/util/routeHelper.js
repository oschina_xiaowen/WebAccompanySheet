/**
 * helper to control route for cross platform
 * @module  util/routeHelper
 */
export default {

    /**
     * get route param
     * @param   {String}  pageQuote   page level object quote, note: app level not any params.
     * @param   {String}  paramName
     * @param   {String}  defaultValue
     * @returns {String}
     */
    getParam : function(pageQuote, paramName = '', defaultValue = null)
    {
        // for Vue framework, use pageQuote.$route.params API
        if (typeof(pageQuote.$route) !== 'undefined')
        {
            if (typeof (pageQuote.$route.params[paramName]) !== 'undefined')
            {
                return pageQuote.$route.params[paramName];
            }
            else
            {
                return defaultValue;
            }
        }

        // for Uniapp, user pageQuote.$root.$mp.query API
        else if (typeof(pageQuote.$root) !== 'undefined')
        {
            if (typeof (pageQuote.$root.$mp.query[paramName]) !== 'undefined')
            {
                return pageQuote.$root.$mp.query[paramName];
            }
            else
            {
                return defaultValue;
            }
        }

        return defaultValue;
    }

}
