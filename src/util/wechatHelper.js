import consoleHelper from './consoleHelper';
import objectHelper from './objectHelper';

/**
 * @module  util/wechatHelper
 */

export default {

    jssdkReady : false,

    /**
     * @param   {Object}    options
     * @param   {String}    options.title  分享标题
     * @param   {String}    options.desc   分享描述
     * @param   {String}    options.link   链接
     * @param   {String}    options.imgUrl 分享图标
     * @param   {String}    options.type   分享类型,music、video或link，不填默认为link
     */
    setShare(options) {

        if ( typeof(wx) === 'undefined' )
        {
            consoleHelper.logError('[error] wx object not defined.');
            return false;
        }

        if (!this.jssdkReady)
        {
            wx.ready(() => {
                this.jssdkReady = true;
                this.setShare(options);
            });
            return;
        }

        if (typeof(wx.updateTimelineShareData) !== 'undefined')
        {
            options = objectHelper.merge(options, {
                success: function (){
                    consoleHelper.log('updateTimelineShareData success.');
                },
                fail : function (){
                    consoleHelper.log('updateTimelineShareData failed.');
                }
            });
            wx.updateTimelineShareData(options);

            options = objectHelper.merge(options, {
                success: function (){
                    consoleHelper.log('updateAppMessageShareData success.');
                },
                fail : function (){
                    consoleHelper.log('updateAppMessageShareData failed.');
                }
            });
            wx.updateAppMessageShareData(options);
        }
        else
        {
            wx.onMenuShareTimeline(options);
            wx.onMenuShareAppMessage(options);
            wx.onMenuShareQQ(options);
            wx.onMenuShareQZone(options);
            wx.onMenuShareWeibo(options);
        }

        if (typeof(wx.miniProgram) !== 'undefined')
        {
            let url = encodeURIComponent(options.link);
            // 重点，share是小程序的页面中，从分享进入的h5的落地页
            let path = '/pages/share/share?data='+JSON.stringify({
                title   : options.title || '',
                desc    : options.desc || '',
                url     : url
            });
            wx.miniProgram.postMessage({
                data: {
                    title: options.title || '',
                    desc: options.desc || '',
                    path: path
                }
            });
        }
    }

}
