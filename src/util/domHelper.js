import consoleHelper from './consoleHelper';

/**
 * helper to operate DOM
 * @module  util/domHelper
 */
export default {

    /**
     * get element by id  根据ID获取元素
     * @param id {String}  element ID
     * @returns {Object(HTMLElement) | null}
     */
    getById(id)
    {
        return document.getElementById(id);
    },

    /**
     * get element by id  根据ID获取元素
     * @param id {String}  element ID
     * @returns {Object(HTMLElement) | null}
     */
    getElement(id)
    {
        return this.getById(id);
    },

    /**
     * get elements
     * @param   {String}  selector  CSS query selector
     * @returns {*}
     */
    get : function(selector)
    {
        try
        {
            if (typeof(document.querySelector) != 'function')
            {
                consoleHelper.log('querySelector is not supported in current client.');
                return false;
            }
            return document.querySelector(selector);
        }
        catch (e)
        {
            return false;
        }
    },

    /**
     * create element  创建元素
     * @param name     {String}  element name
     * @param options  {Object}  element attributes
     * @param content  {Object} of HTMLElement | String  element content
     * @returns {Object(HTMLElement) | false}
     */
    createElement(name, options, content)
    {
        // create element
        const element = document.createElement(name);

        // set attributes
        if (options !== null) {
            for (let attr_name in options) {
                element.setAttribute(attr_name, options[attr_name]);
            }
        }

        // set content
        if (typeof (content) === "object") {
            element.innerHTML = content.outerHTML;
        }
        else if (typeof (content) === 'string' || typeof (content) === 'number') {
            element.innerHTML = content;
        }

        // return
        return element;
    },

    /**
     * generate a unique id for a element, the result is like "element_n" (n is a natural number) format.
     * @param   {String}  name        element type name, default: "element"
     * @param   {String}  delimiter   default: "_"
     * @return  {String}
     */
    generateIdForElement(name = 'element', delimiter = '_')
    {
        let exist   = true;
        let number  = 1;
        let maxNumber   = 999;
        let id = '';
        while (exist && number <= maxNumber)
        {
            id = name + delimiter + number;
            exist = this.getElement(id) != null;
            number++;
        }
        return id;
    },

    /**
     * get target data-* value in a event handler
     * @param   {Object}  event
     * @param   {String}  name
     * @return  {String}
     */
    getTargetDataByEvent : function(event, name)
    {
        const target = event.currentTarget;
        if (typeof(target.dataset[name]) != 'undefined')
        {
            return target.dataset[name];
        }
        return '';
    },

    /**
     * get relative length by click in a wrapper
     * @param   {Object(Event)}   clickEvent
     * @return  {Number}
     */
    getProgressClickPosition(clickEvent)
    {
        const target = clickEvent.currentTarget;
        const targetWidth = target.offsetWidth;
        const targetX = target.offsetLeft;
        const pointX = clickEvent.clientX - targetX;
        const pointXrate = pointX / targetWidth;
        return pointXrate;
    },

    /**
     * get relative length by touch in a wrapper
     * @param   {Object(Event)}   clickEvent
     * @return  {*}     number or false
     */
    getProgressTouchPosition(touchEvent)
    {
        const target = touchEvent.currentTarget;
        const targetWidth = target.offsetWidth;
        const targetX = target.offsetLeft;
        const touches = touchEvent.changedTouches;
        if (touches.length > 1)
        {
            return false;
        }
        const touch = touches[0];
        const pointX = touch.clientX - targetX;
        const pointXrate = pointX / targetWidth;
        return pointXrate;
    },

    /**
     * get view height of document
     * @return {number}
     * refer: https://developer.mozilla.org/zh-CN/docs/Web/API/Element/clientHeight
     */
    getDocumentHeight()
    {
        return document.documentElement.clientHeight || document.body.clientHeight;
    }
};

