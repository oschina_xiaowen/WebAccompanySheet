import animationHelper from './animationHelper';

/**
 * helper to operate window
 * @module  util/windowHelper
 * @author  zhiwen
 */
export default {
    /**
     * get window height  获取窗口的高度
     * @returns {Number}
     */
    getHeight : function()
    {
        // standard API: window.innerHeight
        const height = window.innerHeight
            || window.height;               // IE old version

        return height ? height : 0;
    },

    /**
     * get window width  获取窗口的宽度
     * @returns {Number}
     */
    getWidth : function()
    {
        // standard API: window.innerWidth
        const width = window.innerWidth
            || window.width;            // IE old version

        return width ? width : 0;
    },

    /**
     * Get scroll top  获取窗口视图区域到顶部的滚动距离
     * @returns {Number|Boolean}
     */
    getScrollTop : function()
    {
        if (document.documentElement && document.documentElement.scrollTop)     // For standard
        {
            return document.documentElement.scrollTop;
        }
        else if (document.body)     // For Internet Explorer old version
        {
            return document.body.scrollTop;
        }
        return false;
    },

    /**
     * Get scroll height  获取窗口视图区域的高度
     * @returns {Number|Boolean}
     */
    getScrollHeight : function()
    {
        if (document.documentElement && document.documentElement.scrollHeight)      // For standard
        {
            return document.documentElement.scrollHeight;
        }
        else if (document.body)     // For Internet Explorer
        {
            return document.body.scrollHeight;
        }
        return false;
    },

    /**
     * @param   {Number}    top
     * @param   {Object}    options
     * @param   {Number}    options.duration    set duration to use animation transition
     */
    scrollTo : function(top = 0, options = {})
    {
        const scrollHeight = this.getScrollHeight();
        if (top > scrollHeight)
        {
            top = scrollHeight;
        }

        const current = this.getScrollTop();

        const duration = typeof(options.duration) === 'undefined' ? 300 : options.duration;

        if (duration)
        {
            animationHelper.animate({
                easing : 'easeInOutExpo',
                duration : duration,
                from : current,
                to : top,
                step : function (value) {
                    document.body.scrollTop = value;    // For Safari
                    document.documentElement.scrollTop = value; // For Chrome, Firefox, IE and Opera
                }
            });
        }
        else    // no animation
        {
            document.body.scrollTop = top;    // For Safari
            document.documentElement.scrollTop = top; // For Chrome, Firefox, IE and Opera
        }


    }
};

