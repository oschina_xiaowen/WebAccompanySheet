import consoleHelper from './consoleHelper';
import stringHelper from './stringHelper';

/**
 * helper for number processing and calculate
 */
export default {

    /**
     * @param   {number}    number
     * @param   {number}    bit
     * @return  {string}
     */
    fixFloatingPointNumber : function(number, bit = 2)
    {
        let numberStr = number + '';
        if (numberStr.indexOf('.') < 0) // no point, fill 0
        {
            return numberStr + '.' + stringHelper.repeat('0', bit);
        }

        const a1 = numberStr.split('.');

        if (a1[1].length < bit)
        {
            a1[1] += stringHelper.repeat('0', bit - a1[1].length);
        }
        else if (a1[1].length > bit)
        {
            a1[1] = a1[1].substr(0, bit);
        }

        let result = a1.join('.');
        result = parseFloat(result);

        return result;
    },

    /**
     * random generate a integer number
     * @param  {Number}  length
     * @return {Number}
     */
    getRandomInteger : function(length)
    {
        return (Math.floor(Math.random() * length));
    },

    /**
     * multiply operation for floating number
     * @param   {Number} multiplier
     * @param   {Number} multiplicand
     * @return  {Number}
     */
    multiply : function (multiplier, multiplicand, decimalBit)
    {
        decimalBit = decimalBit ? parseInt(decimalBit) : 0;
        const power = Math.pow(10, decimalBit);
        multiplier      = Math.floor(multiplier * power);
        multiplicand    = Math.floor(multiplicand * power);

        return multiplier * multiplicand / power / power;
    },

    /**
     * plus operation for floating number
     * @param   {Number} addend
     * @param   {Number} addend2
     * @param   {Number} decimalBit
     * @return  {Number}
     */
    plus : function (addend, addend2, decimalBit)
    {
        decimalBit = decimalBit ? parseInt(decimalBit) : 0;
        const power = Math.pow(10, decimalBit);
        addend    = Math.floor(addend * power);
        addend2   = Math.floor(addend2 * power);

        return (addend + addend2) / power;
    },

    /*
     * 判断一个数字是否是质数：
     * 质数（prime number）又称素数，有无限个。除了1和它本身以外不再被其他的除数整除。
     */
    isPrime : function(number){

        //判断输入是否为number类型，是否为整数
        if (typeof number!=='number'||!Number.isInteger(number))
        {
            return false;
        }

        //小于2都不是素数

        if (number<2) {return false}

        //2是素数，被2整除的都不是素数
        if (number===2) {
            return true
        }
        else if(number%2===0) {
            return false;
        }

        //依次判断是否能被奇数整除，最大循环为数值的开方
        const squareRoot = Math.sqrt(number);
        for(let i=3;i<=squareRoot;i+=2) {
            if (number % i === 0) {
                return false;
            }
        }
        return true;
    },

    test : function(){
        for (let i=1; i<=100; i++)
        {
            consoleHelper.log(i + ': ' + (this.isPrime(i) ? 'Yes' : 'No'));
        }
    }

};
