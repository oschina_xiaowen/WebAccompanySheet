/**
 * helper to handle some string.
 * @module  util/stringHelper
 * @author  zhiwen
 */

import arrayHelper from './arrayHelper';

export default {
    /**
     * repeat string
     * @param {String}  string
     * @param {Number}  times
     * @example     stringHelper.repeat('--', 2) => '----'
     */
    repeat : function(string, times)
    {
        let result = "";

        for (let i=1; i<=times; i++)
        {
            result += string;
        }

        return result;
    },

    /**
     * replace string
     * @param string  {String}  The string being searched and replaced on.
     * @param find    {String}  The value being searched for.
     * @param replace {String}  The replacement value that replaces found search values.
     * @returns {String}
     */
    replace : function(string, find, replace)
    {
        if (find == replace) {
            return string;
        }

        let oStr = new String(string);
        while (oStr.indexOf(find) >= 0) {
            oStr = oStr.replace(find, replace);
        }
        return oStr;
    },

    /**
     * replace some data with key
     * @param       string  {String}
     * @param       data    {Object}  key-value pairs
     * @return      {String}
     * @example     stringHelper.replaceData('name: NAME', {NAME : 'Tom'}) => 'name: Tom'
     */
    replaceData : function(string, data)
    {
        for (let p in data) {
            while (string.indexOf(p) >= 0) {
                string = string.replace(p, data[p]);
            }
        }

        return string;
    },

    /**
     * get random chinese name
     * @return {String}
     * @example     stringHelper.getRandomChineseName() => '钱志聪'
     */
    getRandomChineseName : function ()
    {
        const surnameList = [
            '赵','钱','孙','李','周','吴','郑','王',
            '冯','陈','褚','卫','蒋','沈','韩','杨',
            '朱','秦','尤','许','何','吕','施','张',
            '孔','曹','严','华','金','魏','陶','姜'
        ];
        const secondWordList = ['云','化','京','彦','小','志','书','健','思','嘉','明'];
        const thirdWordList = ['腾','东','宏','龙','川','东','福','林','聪','诚','珠'];

        const surname     = arrayHelper.getRandomItem(surnameList);
        const secondWord  = arrayHelper.getRandomItem(secondWordList);
        const thirdWord   = arrayHelper.getRandomItem(thirdWordList);

        return surname + secondWord + thirdWord;
    },

    /**
     * get random nickname
     * @return  {String}
     */
    getRandomNickName : function () {
        const list = [
            '风月斟酒',
            '温柔眼眉',
            '吃不饱的大可爱~',
            '脸美逼遭罪@',
            '承诺i',
            '站在千里之外',
            '島是海哭碎菂訫',
            '叫我小透明',
            '别同情我',
            '库存已久',
            '玖忆惜梦',
            '愿你余生不孤寂',
            '二智少女',
            '半夏微凉、不成殇',
            '爱耍流氓的兔子',
            '半城樱花半城雨﹌',
            '野性随风.',
            '≠无心少女',
            '葬于她心',
            '旁观者',
            '你听我说',
            '不惯与生人识',
            '你并不孤单',
            '青袂宛约',
            '让我盲目',
            '夜夜思量',
            '拖拉机再垃圾也能拖垃圾',
            '你会腻我何必',
            '我可爱炸了.',
            '年少轻狂。',
            '走过的路',
            '占有欲',
            '眠里微光。',
            '一灯孤',
            '几支紫钗挽青丝',
            '遥歌',
            '泠泠七弦上',
            '清風与我',
            '善悪不定夺',
            '温致如猫'
        ];
        return arrayHelper.getRandomItem(list);
    },

    /**
     * highlight topic words
     * @param       {String}            text        a text include some # pair string
     * @param       {String|Array}      classList   class name array or a string use space char separate
     * @return      {String}
     * @example     #逗逼# 啦啦啦 #二逼#  =>  <span class="highlightTextColor">#逗逼#</span> 啦啦啦 <span class="highlightTextColor">#二逼#</span>
     */
    highlightTopicWords : function (text, classList)
    {
        const classStr = typeof(classList) === 'object' ? classList.join(' ') : classList;

        const reg = /(#[^#]*#)/g;
        const replaceStr = '<span class="' + classStr + '">$&</span>';

        text = text.replace(reg, replaceStr);

        return text;
    }

}

