/**
 * @module  util/arrayHelper
 */
export default {

    /**
     * clone array
     * @param array     {Array}
     * @returns {Array}
     */
    clone : function(array)
    {
        let newArray = [];

        for (let index in array)
        {
            newArray[index] = typeof(array[index]) === "object" ? this.clone(array[index]) : array[index];
        }

        return newArray;
    },

    /**
     * clone a object, property's value is according to mapping
     * @example     [{name : 'name'}] and {title : name}    => [{title : 'name'}]
     */
    cloneByPropertyMapping : function(array, propertyMapping)
    {
        let list = [];

        for (let i=0; i<array.length; i++)
        {
            let tItem = array[i];

            let item = {};
            for (let key in propertyMapping)
            {
                let sourceKey = propertyMapping[key];

                item[key] = typeof (tItem[sourceKey]) !== 'undefined' ? tItem[sourceKey] : null;
            }

            list.push(item);
        }

        return list;
    },

    /**
     * check if a value in array  检查数组是否存在某个值
     * @param value     {*}
     * @param array     {Array}
     * @returns {Boolean}
     */
    existElement : function(value, array)
    {
        for (let i = 0; i < array.length; i++) {
            if (array[i] === value) {
                return true;
            }
        }
        return false;
    },

    /**
     * alias name for existElement
     */
    existItem : function(value, array)
    {
        return this.existElement(value, array);
    },

    /**
     * get array index by element in a array
     * @param  array    {Array}
     * @param  element  {*}
     * @return {Number}  if not found, then return -1
     */
    getIndex : function(array, element)
    {
        for (let i = 0; i<array.length; i++)
        {
            if (array[i] === element)
            {
                return i;
            }
        }
        return -1;
    },

    /**
     * remove an element from a array, return a new array.
     * @param  array  {Array}
     * @param  element  {*}
     * @returns {Array}
     */
    removeElement : function(array, element)
    {
        let newArray = [];

        for (let i=0; i<array.length; i++)
        {
            if (array[i] === element)
            {
                continue;
            }

            newArray.push(array[i]);
        }

        return newArray;
    },

    /**
     * remove some element from a array, return a new array.
     * @param  array  {Array}
     * @param  removeArray  {Array}
     * @returns {Array}
     */
    removeElements : function(array, removeArray)
    {
        for (let i=0; i<removeArray.length; i++)
        {
            array = this.removeElement(array, removeArray[i]);
        }
        return array;
    },

    /**
     * alias name for removeElement
     * @param  array  {Array}
     * @param  element  {*}
     * @returns {Array}
     */
    removeItem : function(array, element)
    {
        return this.removeElement(array, element);
    },

    /**
     * get item in a list by id
     * @param  list     {Array}
     * @param  id       {String}
     * @return {null|*}
     */
    getItemById : function(list, id)
    {
        for (let i=0; i<list.length; i++)
        {
            if (list[i].id === id)
            {
                return list[i];
            }
        }
        return null;
    },

    /**
     * get item in a list by column
     * @param  list         {Array}
     * @param  columnName   {String}
     * @param  value        {String}
     * @return {null|Object}
     */
    getItemByColumn : function(list, columnName, value) {
        for (let i=0; i<list.length; i++)
        {
            if (list[i][columnName] === value)
            {
                return list[i];
            }
        }

        return null;
    },

    /**
     * get random item in array
     * @param   list   {Array}
     * @return  *
     */
    getRandomItem : function (list) {
        let i = Math.floor( Math.random() * list.length );
        return list[i];
    },

    /**
     * move a element in a array
     * @param   list        {Array}
     * @param   fromIndex   {Number}
     * @param   toIndex     {Number}
     * @return  {Array}
     */
    moveItem : function (list, fromIndex, toIndex) {

        let movingItem = list[fromIndex];

        if (toIndex >= list.length)  // out of array range
        {
            toIndex = list.length - 1;
        }

        let i = 0;
        // move from head to back
        if(toIndex > fromIndex)
        {
            for (i=fromIndex; i<toIndex; i++)   // start from head, each element move a place to head.
            {
                list[i] = list[i+1];
            }
        }
        // move from back to head
        else if (toIndex < fromIndex)
        {
            for (i=fromIndex; i>toIndex; i--)   // start from back, each element move a place to back
            {
                list[i] = list[i-1];
            }
        }

        list[toIndex] = movingItem;

        return list;
    },

    /**
     * merge 2 array, return a new array
     * @param   {Array} array1
     * @param   {Array} array2
     * @returns {Array}
     */
    merge(array1, array2) {
        let array = [];
        for (let i=0; i<array1.length; i++)
        {
            array.push(array1[i]);
        }
        for (let i=0; i<array2.length; i++)
        {
            array.push(array2[i]);
        }
        return array;
    },

    /**
     * bounce, remove null, undefined, "", 0
     * @param {Array}   array
     */
    bounce(array) {
        // Don't show a false ID to this bouncer.
        return array.filter(function(val){
            return !(!val || val === "");
        });
    }

};
