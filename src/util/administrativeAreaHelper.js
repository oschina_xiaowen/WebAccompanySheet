import arrayHelper from './arrayHelper';

/**
 * @module  util/administrativeAreaHelper
 */
export default {

    list : [],

    /**
     * initialize
     * @param   {Array} list
     */
    init(list) {
        this.list = list;
    },

    /**
     * @param   {String}    name    name of top node like province
     * @returns {Object|null}
     */
    getTopAreaByName(name) {
        return arrayHelper.getItemByColumn(this.list, 'name', name);
    },

    /**
     * find a sub item with name in the parent
     * @param   {Object}    parent
     * @param   {String}    name
     * @returns {Object|null}
     */
    getLowerAreaByName(parent, name) {
        return arrayHelper.getItemByColumn(parent.childList, 'name', name);
    }

};

