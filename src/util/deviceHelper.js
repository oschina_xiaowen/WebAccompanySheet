/**
 * @module  util/deviceHelper
 */
const helper = {

    /**
     * If is android device
     * @member      isAndroid
     * @type        {Boolean}
     */
    isAndroid : false,

    /**
     * If is iPhone device
     * @member      isIphone
     * @type        {Boolean}
     */
    isIphone : false,

    /**
     * If is iPad device
     * @member      isIpad
     * @type        {Boolean}
     */
    isIpad : false,

    /**
     * If is browser app
     * @member      isBrowser
     * @type        {Boolean}
     */
    isBrowser : false,

    /**
     * If is Android app
     * @member      isAndroidApp
     * @type        {Boolean}
     */
    isAndroidApp : false,

    /**
     * If is iPhone app
     * @member      isIphoneApp
     * @type        {Boolean}
     */
    isIphoneApp : false,

    /**
     * OS name
     * @member  os
     * @type    {String}
     */
    os : '',

    /**
     * @member  deviceType
     * @type    {String}
     */
    deviceType : '',    // 'mobile' | 'pc'

    init : function () {

        if (typeof(navigator) === 'undefined')
        {
            return;
        }

        this.isBrowser = true;

        //取得浏览器的userAgent字符串
        const userAgent   = navigator.userAgent;
        this.isAndroid  = userAgent.indexOf("Android") > -1;
        this.isIphone   = userAgent.indexOf("iPhone") > -1;
        this.isIpad     = userAgent.indexOf("iPad") > -1;

        this.isAndroidApp    = /android.*mobile\s\S*$/i.test(userAgent);
        this.isIphoneApp     = /iphone.*mobile\/\S*$/i.test(userAgent);

        if (this.isAndroid || this.isIphone || this.isIpad)
        {
            this.deviceType = 'mobile';
        }
        else
        {
            this.deviceType = 'pc';
        }

        const isWindows   = userAgent.indexOf("Windows") > -1;
        const isLinux     = userAgent.indexOf("Linux") > -1;
        const isMacintosh = userAgent.indexOf("Macintosh") > -1;
        if (isWindows)
        {
            this.os = 'Windows';
        }
        if (isLinux)
        {
            this.os = 'Linux';
        }
        if (isMacintosh)
        {
            this.os = 'Macintosh';
        }
        if (this.isAndroid)
        {
            this.os = 'Android';
        }
        if (this.isIphone || this.isIpad)
        {
            this.os = 'iOS';
        }
    }
};
helper.init();
export default helper;
