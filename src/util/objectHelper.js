import consoleHelper from './consoleHelper'

/**
 * some special handle for object
 * @module  util/objectHelper
 */
export default {

    /**
     * update object's properties
     * @param   {Object}  object
     * @param   {Object}  data    new properties
     */
    updateData : function(object, data)
    {
        for (let key in data)
        {
            object[key] = data[key];
        }
    },

    /**
     * clone object
     * @param   {Object}  object
     * @return  {Object}
     */
    clone : function(object)
    {
        return JSON.parse(JSON.stringify(object));
    },

    /**
     * merge 2 object to a new object
     * @param   {Object}  obj1
     * @param   {Object}  obj2
     * @return  {Object}
     */
    merge : function(obj1, obj2)
    {
        let newObject = {};

        for (let name in obj1)
        {
            newObject[name] = obj1[name];
        }
        for (let name in obj2)
        {
            newObject[name] = obj2[name];
        }

        return newObject;
    },

    /**
     * get object property by key path, it can visit deep property.
     * @example getObjectPropertyByKeyPath(product, 'category.name')
     * @param   {Object}          object
     * @param   {String|Array}    path
     */
    getDataByKeyPath : function(object, path)
    {
        if (path === '')
        {
            consoleHelper.logError('path can not be a empty string.');
            return null;
        }
        const pathQueue = typeof(path) === 'string' ? path.split('.') : path;
        if (typeof(object[pathQueue[0]]) === 'undefined')
        {
            return null;
        }

        let property = object[pathQueue[0]];
        pathQueue.shift();

        if (pathQueue.length)
        {
            return this.getDataByKeyPath(property, pathQueue);
        }

        return property;
    },

    /**
     * set object property by key path, it can set deep property.
     * @example setDataByKeyPath(product, 'category.name', 'cat')
     * @param   {Object}  object
     * @param   {String}  path
     * @param   {*}       value
     */
    setDataByKeyPath : function(object, path, value)
    {
        if (path === '')
        {
            consoleHelper.logError('path can not be a empty string.');
            return null;
        }

        const pathQueue = path.split('.');

        let tNode = object;

        // loop into next level
        for (let i=0; i<pathQueue.length; i++)
        {
            const tKey = pathQueue[i];

            // not leaf node, it must a object
            if (i+1 < pathQueue.length)
            {
                // not a object, then create one.
                if ( typeof (tNode[tKey]) === 'undefined' )
                {
                    tNode[tKey] = {};
                }

                // point to next level
                tNode = tNode[tKey];
            }
            else    // leaf node, set value
            {
                tNode[tKey] = value;
            }
        }
    },

    /**
     * convert different columns name of object
     * example: {created_date : 123, user_name : 'abc'} => {createdDate : 123, userName : 'abc'}
     * @param  {Object}  data
     * @param  {Object}  mapping of columns: newProperty => oldProperty
     * @return {Object}
     */
    dataColumnConvert : function(data, mapping)
    {
        let newData = this.clone(data);

        // change property in mapping
        for (let mapKey in mapping)
        {
            if (mapping[mapKey].indexOf('.') < 0)
            {
                newData[mapKey] = newData[mapping[mapKey]];
                delete newData[mapping[mapKey]];
            }
            else
            {
                newData[mapKey] = this.getDataByKeyPath(newData, mapping[mapKey]);
            }
        }
        return newData;
    },

    /**
     * @param   {Object}  data
     * @param   {Object}  mapping
     * @return  {Object}
     */
    dataColumnConvertReverse : function(data, mapping)
    {
        let newData = {};

        // clone all properties
        for (let key in data)
        {
            newData[key] = data[key];
        }

        // change property in mapping
        for (let mapKey in mapping)
        {
            newData[mapping[mapKey]] = newData[mapKey];
            delete newData[mapKey];
        }

        return newData;
    },

    /**
     * convert a list of object's different columns name
     * example: [{created_date : 123, user_name : 'abc'}] => [{createdDate : 123, userName : 'abc'}]
     * @param  {Array}   list
     * @param  {Object}  mapping of columns: newProperty => oldProperty
     * @return {Object}
     */
    listDataColumnConvert : function(list, mapping)
    {
        let newList = [];
        for (let i=0; i<list.length; i++)
        {
            let newItem = this.dataColumnConvert(list[i], mapping);
            newList.push(newItem);
        }

        return newList;
    },

    /**
     * filter items from list according to object's property.
     * @param   {Array}   list
     * @param   {String}  name    property name
     * @param   {*}   value   property value
     * @return  {Array}
     */
    listDataFilter : function(list, name, value)
    {
        let newList = [];
        for (let i=0; i<list.length; i++)
        {
            if (list[i][name] === value)
            {
                newList.push(list[i]);
            }
        }
        return newList;
    }
};
