import Vue from 'vue'
import App from './App.vue'
import router from '@router/router';
Vue.config.productionTip = false;
Vue.config.ignoredElements = ['wx-open-launch-weapp'];

// Vue plugin
import Pin from 'vue-pin';
Vue.use(Pin)

new Vue({
  el: '#app',
  router,
  render: h => h(App),
}).$mount('#app')
