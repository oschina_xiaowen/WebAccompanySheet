const appConfig = {
    mode : 'dev',
    name : 'Web App Framework',
    desc : '',
    apiHostname : '',
    apiBaseUrl : '',
    wechat : {
        enableJSSDK : false,
        jsApiList: [
            'onMenuShareAppMessage', 'onMenuShareTimeline',
            'onMenuShareQQ', 'onMenuShareWeibo', 'onMenuShareQZone',
            'updateAppMessageShareData', 'updateTimelineShareData',
            'chooseWXPay', 'getLocation'
        ],
        autoLogin : true
    },
    messageMap : {
        networkError : '网络出错，请稍后再试。',
        networkTimeout : '网络通讯超时，请尝试稍后重试。',
        saveSuccess : '保存成功！',
        sessionTimeout : '会话超时',
        permissionDeny : '没有相关操作权限',
        methodNotSupport : '网络通讯失败：method not support',
        serviceError : '网络通讯失败：Service error',
    }
};

let externalAppConfig = {
    testing : {
    },
    production : {
    }
};

// env
if (process.env.NODE_ENV === 'production')
{
    if (process.env.VUE_APP_MODE === 'prod')        // production 生产环境
    {
        appConfig.apiHostname = '';
    }
    else if (process.env.VUE_APP_MODE === 'test')   // test 测试环境
    {
        appConfig.apiHostname = '';
    }
    // appConfig.apiBaseUrl = '//' + appConfig.apiHostname + '//';
}
else if (process.env.NODE_ENV === 'development')  // development 开发环境
{
    appConfig.apiHostname = 'localhost';
    appConfig.apiBaseUrl = '';  // for browser
}

export default appConfig;
