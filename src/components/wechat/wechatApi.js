import requestManager from '../platformApi/requestManager'

export default {

    wechatJSSDK : function(params, callback) {
        requestManager.request({
            url: 'base/wx/jssdkconfig/jssdk',
            params: params
        }, callback);
    }
};
