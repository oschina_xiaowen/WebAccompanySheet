import modulePart from './modulePart'

// api
import adApi  from '../../api/ad';

// platformApi
import requestManager from "../platformApi/requestManager";

import objectHelper from '../../util/objectHelper';
import consoleHelper from '../../util/consoleHelper';
import appHelper from "@/components/appHelper";

/**
 * @module  part/adModulePart
 */

export default {

    extends : modulePart,

    props : {
        /**
         * (component property) as limit param to load data
         * @member      itemCount
         * @example     <adModule itemCount="3"/>
         */
        itemCount : {
            type : Number,
            default : 10
        },
        /**
         * (component property) as typeEnum param to load data
         * @member      type
         * @example     <adModule type="HOME"/>
         */
        type : {
            type : String | Number,
            default : ''
        },
        version : {
            type : String,
            default : '1'
        }
    },
    data () {
        return {
            /**
             * @var  {String}
             */
            status : '',    // '' | 'ready' | 'error'

            /**
             * one ad data
             * @var {Null|Object}
             */
            adData : null,

            /**
             * ad list
             * @var {Array}
             */
            adList : [],

            /**
             * api config for loading ad data
             * @var {Object}
             */
            apiConfig : {
                list : {
                    url : ''
                }
            }
        }
    },
    methods : {

        /**
         * load ad data with some params
         * @function    loadAd
         * @param       {Object}    params
         * @param       {Function}  callback
         */
        loadAd : function (params = {}, callback) {

            let requestOptions = {
                url : '',
                params : params
            };
            if (this.version >= '4')
            {
                requestOptions.url = adApi.getV4.url;
            }
            else
            {
                requestOptions.url = adApi.getV1.url;
            }

            const dataPath = this.apiConfig.list.dataPath || 'data';
            requestManager.request(requestOptions, (error, result) => {
                if (error)
                {
                    this.status = 'error';
                    callback ? callback(error, null) : null;
                    return;
                }

                this.status = 'ready';
                let data = objectHelper.getDataByKeyPath(result, dataPath);
                typeof (data.length) == 'number' ?
                    this.adList = data :
                    this.adData = data;
                callback ? callback(null, data) : null;
                this.onLoadData(data);
            });
        },

        /**
         * submit click log, and redirect to new path.
         * @method  clickAd
         * @param   {Object}    item    ad data
         */
        clickAd : function (item) {

            consoleHelper.logDebug('click ad.');
            adApi.click(item.id, () => {
                appHelper.redirect(item.url);
            });
        },

        onLoadData : function() {

        }

    },

    mounted() {

        if (this.type) {
            this.loadAd({
                typeEnum : this.type,
                limit : this.itemCount
            });
        }
    }

}

