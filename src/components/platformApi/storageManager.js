/**
 * @module  platformApi/storageManager
 */
let manager = {

    name : 'storageManager',

    /**
     * set a storage item
     * @method  setItem
     * @param   {String}  key
     * @param   {String}  value
     * @param   {Object}  options
     * @param   {String}  options.expire    ''(default)|'session'
     * @description     as same as localStorage.setItem
     */
    setItem : () => {},

    /**
     * get a storage item
     * @method  getItem
     * @param   {String}  key
     * @return  {String|null}
     * @description     as same as localStorage.getItem
     */
    getItem : () => {},

    /**
     * remove a storage item
     * @method  removeItem
     * @param   {String}  key
     * @description     as same as localStorage.removeItem
     */
    removeItem : () => {},

    /**
     * clear storage
     * @method  clear
     * @description     as same as localStorage.clear
     */
    clear : () => {}

};

if (typeof(navigator) !== 'undefined')  // browser platform
{
    manager.setItem = function (key, value, options = {}) {

        const expire = options.expire || '';
        if (expire === 'session')
        {
            if (typeof (sessionStorage) !== 'undefined')  // most browser support
            {
                sessionStorage.setItem(key, value);
                return;
            }
        }
        else
        {
            if (typeof (localStorage) !== 'undefined')  // most browser support
            {
                localStorage.setItem(key, value);
                return;
            }
        }
    };

    manager.getItem = function (key) {

        let value = null;

        if (typeof (localStorage) !== 'undefined')  // most browser support
        {
            value = localStorage.getItem(key);
        }
        if (!value)
        {
            value = sessionStorage.getItem(key);
        }
        return value;
    };

    manager.removeItem = function (key) {

        if (typeof (localStorage) !== 'undefined')
        {
            localStorage.removeItem(key);
            sessionStorage.removeItem(key);
        }
    };

    manager.clear = function () {
        if (typeof (localStorage) !== 'undefined')
        {
            localStorage.clear();
            sessionStorage.clear();
        }
    };
}

else if ( typeof (wx) !== 'undefined' &&
    typeof(wx.getStorageSync) == 'function' )    // wechat mini program platform
{
    manager.setItem = function (key, value) {
        wx.setStorageSync(key, value);
    };

    manager.getItem = function (key) {
        return wx.getStorageSync(key);
    };

    manager.removeItem = function (key) {
        wx.removeStorageSync(key);
    };

    manager.clear = function () {
        wx.clearStorageSync();
    };
}

export default manager;
