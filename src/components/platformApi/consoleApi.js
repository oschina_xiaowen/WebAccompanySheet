/**
 * @module  platformApi/consoleApi
 */
export default {

    /**
     * console object quote
     * @member  console
     * @var {Object|Null}
     */
    console : (function(){

        // for modern browser and some mini program
        if (typeof (console) === 'object')
        {
            return console;
        }
        // for old version browser or 3rd-party console lib
        else if (typeof(window) === 'object' && typeof (window.console) === 'object')
        {
            return window.console;
        }

        return null;
    })(),

    /**
     * @method  log
     * @description     as same as console.info, unlimit param.
     */
    log : function()
    {
        this.console ? this.console.log(...arguments) : null;
    },

    /**
     * @method  info
     * @description     as same as console.info, unlimit param.
     */
    info : function()
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.info) != 'undefined')   // some old version browser maybe not support.
        {
            this.console.info(...arguments);
        }
        else
        {
            this.console.log(...arguments);
        }
    },

    /**
     * @method  warn
     * @description     as same as console.info, unlimit param.
     */
    warn : function()
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.warn) != 'undefined')   // some old version browser maybe not support.
        {
            this.console.warn(...arguments);
        }
        else
        {
            this.console.log(...arguments);
        }
    },

    /**
     * @method  error
     * @description     as same as console.info, unlimit param.
     */
    error : function()
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.error) != 'undefined')  // some old version browser maybe not support.
        {
            this.console.error(...arguments);
        }
        else
        {
            this.console.log(...arguments);
        }
    },

    /**
     * @method  group
     * @description     as same as console.group
     */
    group : function()
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.group) != 'undefined')  // some old version browser maybe not support.
        {
            this.console.group();
        }
    },

    /**
     * @method  groupEnd
     * @description     as same as console.groupEnd
     */
    groupEnd : function()
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.groupEnd) != 'undefined')   // some old version browser maybe not support.
        {
            this.console.groupEnd();
        }
    },

    /**
     * @method  time
     * @param   {String}    timerName
     * @description     as same as console.time
     */
    time : function(timerName)
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.time) != 'undefined')   // some old version browser maybe not support.
        {
            this.console.time(timerName);
        }
    },

    /**
     * @method  timeEnd
     * @param   {String}    timerName
     * @description     as same as console.timeEnd
     */
    timeEnd : function(timerName)
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.timeEnd) != 'undefined')    // some old version browser maybe not support.
        {
            this.console.timeEnd(timerName);
        }
    },

    /**
     * @method  table
     * @param   {Array}    list
     * @description     as same as console.table
     */
    table : function(list)
    {
        if (!this.console)
        {
            return;
        }

        if (typeof(this.console.table) != 'undefined')  // some old version browser maybe not support.
        {
            this.console.table(list);
        }
    },

    test : function (){
        this.log('log');
        this.info('info');
        this.warn('warn');
        this.error('error');
        this.table([1,2,3,4,5]);
        this.table([{
            log : this.log,
            info : this.info,
            warn : this.warn,
            error : this.error
        }]);
    }
};
