/**
 * @module  platformApi/timerManager
 */
const manager = {

    name : 'timerManager',

    /**
     * @method  setInterval
     * @param   {Function}    stepFunc
     * @param   {Number}      delay     delay millisecond
     * @return  {Number}      timer id
     * @description     as same as window.setInterval()
     */
    setInterval : (stepFunc, delay) => {

    },

    /**
     * @method  clearInterval
     * @param   {Number}  timer instance id
     * @description     as same as window.clearInterval()
     */
    clearInterval : (timerId) => {},

    /**
     * @method  setTimeout
     * @param   {Function}    stepFunc
     * @param   {Number}      delay     delay millisecond
     * @return  {Number}      timer id
     * @description     as same as window.setTimeout()
     */
    setTimeout : (stepFunc, delay) => {},

    /**
     * @method  clearTimeout
     * @param   {Number}  timer instance id
     * @description     as same as window.clearTimeout()
     */
    clearTimeout : (timerId) => {},

};

if (typeof(navigator) !== 'undefined')  // browser platform
{
    manager.setInterval = function (stepFunc, delay) {
        return window.setInterval(stepFunc, delay);
    };

    manager.clearInterval = function (timerId) {
        window.clearInterval(timerId);
    };

    manager.setTimeout = function (stepFunc, delay) {
        return window.setTimeout(stepFunc, delay);
    };

    manager.clearTimeout = function (timerId) {
        window.clearTimeout(timerId);
    };
}

else if ( typeof(setInterval) == 'function' )    // wechat mini program platform
{
    manager.setInterval = function (stepFunc, delay) {
        return setInterval(stepFunc, delay);
    };

    manager.clearInterval = function (timerId) {
        clearInterval(timerId);
    };

    manager.setTimeout = function (stepFunc, delay) {
        return setTimeout(stepFunc, delay);
    };

    manager.clearTimeout = function (timerId) {
        clearTimeout(timerId);
    };
}

export default manager;
