import deviceHelper from '@util/deviceHelper';
import browserHelper from '@util/browserHelper';
/**
 * @module  platformApi/paymentManager
 */
const manager = {

    name : 'paymentManager',

    /**
     * get payment way
     * @method  getPaymentWay
     * @return  {String}
     */
    getPaymentWay : function (){ return ''; },

};

if (typeof(navigator) !== 'undefined')  // browser platform
{
    /**
     * @return  {String}    SCAN|ANDROID|IOS|H5  default: H5
     * PC       -> scan qr code to pay
     * ANDROID  -> call Android API
     * IOS      -> call iOS API
     * H5       -> call wechat payment API
     */
    manager.getPaymentWay = (function ()
    {
        if (deviceHelper.deviceType === 'pc'){  // scan payment Qr code
            return 'SCAN';
        }

        if (browserHelper.isWechatAppWebview)  // call Apple payment or other payment app
        {
            return 'WX_MINI';
        }

        if (deviceHelper.isAndroidApp)     // call Android payment or other payment app
        {
            return 'ANDROID';
        }
        else if (deviceHelper.isIphoneApp)  // call Apple payment or other payment app
        {
            return 'IOS';
        }

        return 'H5'     // call wechat webpage payment or other web payment platform.
    });
}

else if ( typeof(setInterval) == 'function' )    // wechat mini program platform
{
    manager.getPaymentWay = (function ()
    {
        return 'WX_MINI';
    });
}

export default manager;
