import storageManager from './storageManager';

import axios from 'axios';
import consoleHelper from "@/util/consoleHelper";

/**
 * @module  platformApi/requestManager
 */
const manager = {

    name : 'requestManager',

    /**
     * network request through http protocol
     * @method  request
     * @param   {Object}        options
     * @param   {String}        options.url       url (required)
     * @param   {String}        options.method    default "GET"
     * @param   {Object}        options.data      extra request data
     * @param   {Function}      callback
     * @param   {Object|null}   callback.error          not null if has error
     * @param   {Object|null}   callback.response data  not null if has no error
     */
    request : () => {},

    /**
     * upload file request through http protocol
     * @method  uploadFile
     * @param   {Object}        options
     * @param   {String}        options.url       url (required)
     * @param   {String}        options.method    default "GET"
     * @param   {Object}        options.data      extra request data
     * @param   {Object|String} file object for browser or file path for wechat mini app
     * @param   {Function}      callback
     * @param   {Object|null}   callback.error          not null if has error
     * @param   {Object|null}   callback.response data  not null if has no error
     */
    uploadFile : () => {

    }
};

if (typeof(navigator) !== 'undefined')              // for browser platform
{
    manager.request = (function(options = {}, callback = null)
    {
        const method = options.method || 'get';

        if ( (method === 'get' || method === 'put') && typeof (options.data) !== 'undefined') {
            options.params = options.data;
            delete options.data;
        }

        // more params refer to <a href="https://github.com/axios/axios#axios-api" target="blank">https://github.com/axios/axios#axios-api</a>
        axios(options).then(res => {

            if (!callback)
            {
                return;
            }

            try
            {
                callback(null, res.data);
            }
            catch(e)
            {
                consoleHelper.logError(e);
            }

        }).catch(error => {
            callback ? callback(error, null) : null;
        });
    });

    manager.uploadFile = (function(options = {}, file = null, callback = null)
    {
        options.method  = options.method || 'POST';
        let name        = options.name || 'file';
        let data        = options.data || {};

        // build form data
        let formData = new FormData();
        for (let key in data)
        {
            formData.append(key, data[key]);
        }
        formData.append(name, file);

        options.data = formData;

        let request = axios.create({
            headers : {
                'Content-Type' : 'multipart/form-data;'
            },
            withCredentials : true
        });

        request(options).then(res => {

            if (!callback)
            {
                return;
            }

            try
            {
                callback(null, res.data);
            }
            catch(e)
            {
                consoleHelper.logError(e);
            }

        }).catch(error => {
            callback ? callback(error, null) : null;
        });
    });
}

if (typeof(wx) !== 'undefined' && typeof(wx.request) !== 'undefined')   // for wechat mini application
{
    manager.request = (function(options = {}, callback = null)
    {
        let method = new String(options['method']  || 'get').toLowerCase();
        let header = options['header']  || {};

        // set default content-type
        if (method == 'post' || method == 'put' || method == 'delete')
        {
            let contentType = header['content-type']  || '';
            if (!contentType)
            {
                header['content-type'] = 'application/x-www-form-urlencoded;charset=UTF-8';
            }
        }
        else if ( method === 'get' && typeof (options.params) !== 'undefined') {
            options.data = options.params;
            delete options.params;
        }

        let cookie = storageManager.getItem('cookie');
        if (cookie)
        {
            header['cookie'] = cookie;
        }

        options.header = header;

        /**
         * @type    Function
         * @param   string|Object|Arraybuffer   data
         * @param   Number                      statusCode
         * @param   Object                      header
         * @param   Array<string>               cookies
         * @param   Object                      profile
         */
        options.success = (function(res){

            if (!callback)
            {
                return;
            }

            try
            {
                callback(null, res.data, res);
            }
            catch(e)
            {
                consoleHelper.logError(e);
            }

        });
        options.fail = (function(error){
            callback ? callback(error, null) : null;
        });

        wx.request(options);
    });

    manager.uploadFile = (function(options = {}, file = null, callback = null)
    {
        options.filePath = file;

        // data change to formData
        options.formData = options.data || {};
        delete options.data;

        /**
         * @type    Function
         * @param   string|Object|Arraybuffer   data
         * @param   Number                      statusCode
         * @param   Object                      header
         * @param   Array<string>               cookies
         * @param   Object                      profile
         */
        options.success = (function(res){

            if (!callback)
            {
                return;
            }

            try
            {
                callback(null, res.data);
            }
            catch(e)
            {
                consoleHelper.logError(e);
            }
        });
        options.fail = (function(error){
            callback ? callback(error, null) : null;
        });

        wx.uploadFile(options);
    });
}

export default manager;

