/**
 * @module  platformApi/timerManager
 */

import browserHelper from '../../util/browserHelper';

const manager = {

    name : 'locationManager',

    /**
     * @param   {Object}    options
     * @param   {String}    options.type   location data type, default wgs84.
     * @param   {Function}  callback
     */
    getLocation : (options = {}, callback) => {
        options;
    }

};

if (typeof(navigator) !== 'undefined')  // browser platform
{
    if (browserHelper.isWechat)
    {
        manager.getLocation = (options = {}, callback) =>
        {
            let type    = options.type      || 'wgs84';

            wx.getLocation({
                type: type, // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                success: function (res) {
                    let latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                    let longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                    let speed = res.speed; // 速度，以米/每秒计
                    let accuracy = res.accuracy; // 位置精度
                    callback(null, res);
                },
                error : function (res){
                    callback(res, null);
                }
            });
        }
    }
    else
    {
        manager.getLocation = (options = {}, callback) =>
        {
            let type    = options.type      || 'wgs84';
            let success = options.success   || null;
            let error   = options.error     || null;

            navigator.geolocation.getCurrentPosition(
                //locationSuccess
                function(result){
                    callback(null, result.coords);
                },
                //locationError
                function(result){
                    let errorType = ['您拒绝共享位置信息', '获取不到位置信息', '获取位置信息超时'];
                    callback(result, null);
                }
            );
        }

    }
}

else if ( typeof(setInterval) == 'function' )    // wechat mini program platform
{
    manager.getLocation = function (options = {}, callback)
    {
        let type    = options.type      || 'wgs84';
        let success = options.success   || null;
        let error   = options.error     || null;

        wx.getLocation({
            type: type, // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
            success: function (res) {
                let latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                let longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                let speed = res.speed; // 速度，以米/每秒计
                let accuracy = res.accuracy; // 位置精度
                let altitude = res.altitude; // 高度
                let verticalAccuracy = res.verticalAccuracy; // 垂直精度
                let horizontalAccuracy = res.horizontalAccuracy; // 水平精度
                callback(null, position);
            },
            error : function (res){
                callback(result, null);
            }
        })
    };
}

export default manager;
