import generalCmpPart from './generalCmpPart';

/**
 * 模块零件
 * @description     模块继承该类，包含模块常用属性和方法
 * @module          part/modulePart
 */
export default {

    extends : generalCmpPart,
    props : {
        /**
         * (tag property) module title
         * @member  {String}  title
         */
        title : {
            type : String,
            default : ''
        },
        titleStyle : String,
        /**
         * (tag property) module description
         * @member  {String}  desc
         */
        desc : {
            type : String,
            default : ''
        },
        descStyle : String,
        /**
         * (tag property) module content layout
         * @member  {{String}}  layout
         */
        layout : {
            type : String,
            default : 'newsListLayout'
        },
        /**
         * (tag property) content item count
         * @member  {Number}  itemCount
         */
        itemCount : {
            type : Number,
            default : 4
        },
        /**
         * (tag property) content data params
         * @member  {Object}  dataParams
         */
        dataParams : {
            type : Object,
            default : () => {
                return {}
            }
        }

    }

}

