import generalCmpPart from './generalCmpPart';
/**
 * 抽屉组件零件
 * @module  part/drawerPart
 */
export default {
    extends : generalCmpPart,
    data() {
        return {
            /**
             * @var {Boolean}   visible status
             */
            visible : false
        }
    },
    methods : {
        open : function () {
            this.visible = true;
            this.$emit('open');
        },
        close : function () {
            this.visible = false;
            this.$emit('close');
        }
    }
};
