import generalCmpPart from './generalCmpPart';

// util
import appHelper        from "../appHelper";
import consoleHelper    from "../../util/consoleHelper";
import windowHelper     from "../../util/windowHelper";

/**
 * 页面零件
 * @description     页面继承该类，可以自动设置页面标题
 * @module          part/pagePart
 */
export default {

    extends : generalCmpPart,
    data() {
        return {
            /**
             * page title
             * @var {String}
             */
            title : '',

            /**
             * page description
             * @var {String}
             */
            description : '',

            /**
             * page params from url
             * @var {Object}
             */
            params : {},

            /**
             * scroll control
             * @member  scrollControl
             * @type    {Object}
             */
            scrollControl: {
                /**
                 * @type    {window|HTMLElement}
                 */
                viewContainer: window || null,
                /**
                 * @type    {documentElement|HTMLElement}
                 */
                scrollContainer: document ? document.documentElement : null
            },

            scrollToLoadNextPage : 40,

            windowEventMap : {}

        }
    },
    methods : {

        /**
         * @method  getScrollContainer
         * @return  {documentElement|HTMLElement}
         */
        getScrollContainer: function () {
            if (typeof (this.scrollControl.scrollContainer) == 'string') {
                return document.querySelector(this.scrollControl.scrollContainer);
            }

            return this.scrollControl.scrollContainer;
        },

        /**
         * @nethod  getViewContainer
         * @return  {Object}     window | HTMLElement
         */
        getViewContainer: function () {
            if (typeof (this.scrollControl.viewContainer) == 'string') {
                return document.querySelector(this.scrollControl.viewContainer);
            }

            return this.scrollControl.viewContainer;
        },

        // onLoading() {
        //     appHelper.showLoading();
        // },
        // onLoadDataList() {
        //     appHelper.hideLoading();
        // },
        // onLoadDataItem() {
        //     appHelper.hideLoading();
        // },

        onHttpError : (error) => {
            appHelper.generalAjaxError(error)
        },

        onError: (result) => {
            appHelper.showHint(result.msg);
        },

        /**
         * on scroll event
         * @method  onScroll
         * @param   {Object}     ScrollEvent
         */
        onScroll : function(event) {

            let me = this;

            if (typeof(me) === 'undefined')
            {
                consoleHelper.log('me is undefined, maybe current component is recrecycled.');
                return;
            }

            if (me.scrollToLoadNextPage === false) {
                consoleHelper.logDebug('scrollToLoadNextPage disabled.');
                return;
            }

            let bottomDistance = me.scrollToLoadNextPage;

            let viewContainer = this.getViewContainer();
            let scrollContainer = this.getScrollContainer();

            // check container
            if (viewContainer == null)      // may be already leave current page.
            {
                consoleHelper.logError('viewContainer not found.');
                return;
            }
            if (scrollContainer == null)    // may be already leave current page.
            {
                consoleHelper.logError('scrollContainer not found.');
                return;
            }

            let viewContainerHeight = 0;
            let scrollContainerHeight = 0;
            let scrollContainerTop = 0;

            if (viewContainer === window)
            {
                viewContainerHeight = viewContainer.innerHeight || viewContainer.offsetHeight;
                scrollContainerHeight = scrollContainer.offsetHeight;
                scrollContainerTop = windowHelper.getScrollTop() || scrollContainer.scrollTop;
                // consoleHelper.logDebug('viewContainerHeight: ' + viewContainerHeight +
                //     ' scrollContainerHeight: ' + scrollContainerHeight +
                //     ' scrollContainerTop: ' + scrollContainerTop);

                // check current element is display
                if (!scrollContainerHeight) {
                    consoleHelper.logDebug('scroll container height: ' + scrollContainerHeight);
                    return;
                }

                // consoleHelper.logDebug('bottom distance: ' + (scrollContainerHeight - scrollContainerTop - viewContainerHeight));
                if (scrollContainerHeight - scrollContainerTop - viewContainerHeight <= bottomDistance) {
                    me.onReachBottom();
                }
            }
            else
            {
                viewContainerHeight     = viewContainer.offsetHeight;
                scrollContainerHeight   = scrollContainer.scrollHeight || scrollContainer.offsetHeight;
                scrollContainerTop      = viewContainer.scrollTop;
                // consoleHelper.logDebug('viewContainerHeight: ' + viewContainerHeight +
                //     ' scrollContainerHeight: ' + scrollContainerHeight +
                //     ' scrollContainerTop: ' + scrollContainerTop);

                // check current element is display
                if (!scrollContainerHeight) {
                    consoleHelper.logDebug('scroll container height: ' + scrollContainerHeight);
                    return;
                }

                // consoleHelper.logDebug('bottom distance: ' + (scrollContainerHeight - scrollContainerTop - viewContainerHeight));
                if (scrollContainerHeight - scrollContainerTop - viewContainerHeight <= bottomDistance) {
                    me.onReachBottom();
                }
            }
        },

        /**
         * window resize event handler
         * @event
         */
        onWindowResize : () => {},

        /**
         * @event
         */
        onReachBottom : function() {
            this.loadNextPage()
        }
    },
    mounted () {

        if (this.title)
        {
            appHelper.setTitle(this.title);
        }

        // get route params
        if (typeof(this.$route) !== 'undefined')
        {
            this.params = this.$route.params || {};
        }

        if (this.enableDataList && this.scrollToLoadNextPage !== false)
        {
            let viewContainer = this.getViewContainer();
            this.windowEventMap.scroll = () => {
                this.onScroll();
            }
            viewContainer ? viewContainer.addEventListener('scroll', this.onScroll) : null;
        }

        // set window resize event for browser
        if (typeof (window) === 'object')
        {
            this.windowEventMap.resize = () => {
                this.onWindowResize();
            }
            window.addEventListener('resize', this.windowEventMap.resize);
        }
    },

    destroyed() {
        // consoleHelper.log('destroy page ' + this.title);

        if (typeof(this.windowEventMap.resize) !== 'undefined')
        {
            window.removeEventListener('resize', this.windowEventMap.resize);
        }

        if (typeof(this.windowEventMap.resize) !== 'undefined')
        {
            window.removeEventListener('resize', this.windowEventMap.resize);
        }

        if (this.enableDataList && this.scrollToLoadNextPage !== false)
        {
            let viewContainer = this.getViewContainer();
            viewContainer ? viewContainer.removeEventListener('scroll', this.windowEventMap.scroll) : null;
        }
    }

}

