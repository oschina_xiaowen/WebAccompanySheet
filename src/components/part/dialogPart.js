/**
 * 对话框组件零件
 * @module  part/dialogPart
 */
export default {
    data() {
        return {
            /**
             * visible status
             * @var {Boolean}
             */
            visible : false
        }
    },
    methods : {
        show : function () {
            this.visible = true;
        },
        hide : function () {
            this.visible = false;
        }
    }
};
