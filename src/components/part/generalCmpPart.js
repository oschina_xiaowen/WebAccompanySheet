// lib
import moment from 'moment';

// platformApi
import requestManager from '../platformApi/requestManager';

// util
import consoleHelper    from '../../util/consoleHelper';
import objectHelper     from '../../util/objectHelper';
import htmlHelper       from '../../util/htmlHelper';
import windowHelper     from "../../util/windowHelper";
import appHelper        from "../appHelper";

/**
 * 通用组件零件
 * 包含：显示控制（用于抽屉、对话框等），输出信号，数据列表功能，数据项目功能
 * @module  part/generalCmpPart
 */

export default {
    data() {
        return {

            /**
             * @member  {Boolean}
             */
            visible : false,

            /**
             * @member  apiConfig
             * @type    {Object}
             * @description     key name is action type, values: 'list', 'categoryList', 'get', 'update', 'remove'. <br/>
             * value is a object that define api options. <br/>
             * common options include 'url', 'method'. <br/>
             * data api options include 'dataPath', 'dataColumnMapping'. <br/>
             * list api options include 'params', 'pageName', 'pageSizeName', 'totalPage'
             */
            apiConfig : {
                list : {    // api for get data list
                    url : '',
                    // method : 'get',
                    // params : {},
                    // pageName : 'page',
                    // pageSizeName : 'limit',
                    // dataPath : 'data.rows',
                    // totalPage : 'data.totalPage'
                },
                categoryList : {
                    url : '',
                },
                get : {     // api for get data detail
                    url : '',
                    // dataColumnMapping : {}
                },
                update : {
                    url : '',
                },
                ad : {
                    url : '',
                },
                remove : {
                    url : '',
                },
            },

            /**
             * @type    {String}
             */
            primaryKey : 'id',

            /**
             * list type data from list api
             * @type    {Array}
             */
            dataList : [],

            /**
             * item type data from get api
             * @type    {Object|null}
             */
            dataItem : null,

            /**
             * category type data from categoryList api
             * @type    {Array}
             */
            categoryList : [],

            /**
             * enable data list functions, for enable autoLoadDataList and auto load next page.
             * @type    {Boolean}
             */
            enableDataList : false,

            /**
             * enable data item functions
             * @type    {Boolean}
             */
            enableDataItem : false,

            /**
             * @var             {String}
             * @description     values: 'loading' | 'ready' | 'error'
             */
            status : '',

            errorInfo : {
                status : 0,
                message : ''
            },

            // list data query config
            pageSize : 10,
            pagination : {
                current : 1,
                pageSize : 10,
                total : 0,
                totalPage : 1,

                /**
                 * @type    Boolean
                 */
                showSizeChanger : true,

                /**
                 * on change size
                 * @type    Function
                 */
                showSizeChange : function(){},

                /**
                 * on change page
                 * @type    Function
                 */
                change : function(){}
            },
            filters : {
                /*
                name : '',
                status : '',
                createTime : ''
                 */
            },

            /**
             * search inputbox setting
             */
            searchInputbox : {
                column : ''     // column name
            },

            /**
             * enable auto load data list
             * @type    {Boolean}
             */
            autoLoadDataList : false,

            /**
             * enable auto load data item
             * @type    {Boolean}
             */
            autoLoadDataItem : false,

            /**
             * map to external controller, it can output some signals.
             * @member
             */
            outputMap : {},

            suitableWidthRatio : 0.8
        }
    },
    methods : {

        // visible control methods

        show : function () {
            this.visible = true;
        },
        hide : function () {
            this.visible = false;
        },
        open : function () {
            this.visible = true;
        },
        close : function () {
            this.visible = false;
        },

        getSuitableWidth : function () {
            let w = windowHelper.getWidth();
            return w * this.suitableWidthRatio;
        },

        // data list operation methods

        /**
         * 加载数据列表
         * @description     需要配置 apiConfig.list
         * @function        loadDataList
         */
        loadDataList: function () {

            if (this.status === 'loading')
            {
                consoleHelper.logDebug('It\'s loading.');
                return;
            }

            let me = this;

            // clone api extra params
            let data = objectHelper.clone(this.apiConfig.list.params || {});

            // add filter's params
            // data = objectHelper.merge(data, this.filters);
            for (let key in this.filters)
            {
                if (this.filters[key])
                {
                    data[key] = this.filters[key];
                }
            }

            let pageName;
            typeof (this.apiConfig.list.pageName) !== 'undefined' ?
                pageName = this.apiConfig.list.pageName :
                pageName = 'page';

            let pageSizeName    = this.apiConfig.list.pageSizeName || 'limit';

            let dataPath        = this.apiConfig.list.dataPath  || 'data.rows';
            let totalPath       = this.apiConfig.list.totalPath || 'data.total';
            let totalPagePath       = this.apiConfig.list.totalPagePath     || 'data.totalPage';
            let dataColumnMapping   = this.apiConfig.list.dataColumnMapping || {};

            // add page params
            if (pageName != null)
            {
                data[pageName]      = this.pagination.current;
                data[pageSizeName]  = this.pageSize;
            }

            // consoleHelper.logDebug('request data:');
            // consoleHelper.logDebug(data);

            let url     = this.apiConfig.list.url || '';
            let method  = this.apiConfig.list.method || 'get';
            // let params  = this.apiConfig.list.params || {};
            // params = objectHelper.merge(params, data);
            consoleHelper.logDebug('loadData params:');
            consoleHelper.logDebug(data);

            if (!url)
            {
                consoleHelper.logWarn('list api url not define, it can not load data.');
                return;
            }

            this.status = 'loading';
            this.onLoading ? this.onLoading() : null;

            let options = {
                url: url,
                method: method
            };
            if (method == 'get')
            {
                options.params = data;
            }
            else
            {
                options.data = data;
            }

            requestManager.request(options, (error, result) => {
                if (error) {
                    this.status = 'error';
                    this.onHttpError(error);
                    return;
                }

                // if (result.errorCode != 0)
                // {
                //     this.status = 'error';
                //     this.onError(result);
                //     return;
                // }

                this.status = 'ready';
                let data        = objectHelper.getDataByKeyPath(result, dataPath);
                let total       = objectHelper.getDataByKeyPath(result, totalPath);
                let totalPage   = objectHelper.getDataByKeyPath(result, totalPagePath);
                if (typeof(data) == 'undefined' || data == null)
                {
                    consoleHelper.logError('data is empty.');
                    return;
                }
                data = objectHelper.listDataColumnConvert(data, dataColumnMapping);

                me.dataList             = me.dataList.concat(data);
                me.pagination.total     = total;
                me.pagination.totalPage = totalPage;

                // prevent trigger request error callback
                try {
                    this.onLoadDataList(data, result);
                }
                catch (e)
                {
                    consoleHelper.logError(e);
                }
            });
        },

        /**
         * 重新加载数据列表
         * @description     加在前会先清空数据
         * @function        reloadDataList
         */
        reloadDataList : function (){

            if (this.status === 'loading')
            {
                consoleHelper.logDebug('It\'s loading.');
                return;
            }

            this.dataList = [];
            this.pagination.current = 1;
            this.loadDataList();
        },

        /**
         * 加载下一页数据
         * @function    loadNextPage
         */
        loadNextPage : function () {

            consoleHelper.logDebug('load next page.');
            if (this.status === 'loading')
            {
                consoleHelper.logDebug('current status is loading, return.');
                return;
            }

            if (this.pagination.current >= this.pagination.totalPage)
            {
                consoleHelper.logDebug("It's already last page.");
                return;
            }

            this.pagination.current++;
            this.loadDataList();

        },

        /*
         * methods for pagination
         */

        /**
         * @event   onChangePage
         * @param   {Number}  page
         * @param   {Number}  pageSize
         */
        onChangePage: function (page, pageSize) {
            consoleHelper.logDebug('page=' + page + ' pageSize=' + pageSize);
            this.pagination.current = page;
            this.dataList = [];
            this.loadDataList();
        },
        /**
         * @event   onChangeSize
         * @param   {Number}    current
         * @param   {Number}    size
         */
        onChangeSize: function (current, size) {
            consoleHelper.logDebug('size=' + size);
            this.pagination.current = 1;
            this.pageSize = this.pagination.pageSize = size;
            this.dataList = [];
            this.loadDataList();
        },

        /*
         * methods for table
         */

        // pagination, filters, sorter, { currentDataSource })
        onTableChange : function (pagination){
            consoleHelper.logDebug(pagination);
            this.pagination.current = pagination.current;
            this.pagination.pageSize = pagination.pageSize;
            this.dataList = [];
            this.loadDataList();
        },

        /**
         * methods for operation data item
         */

        /**
         * 重置数据项目 dataItem
         * @method  resetDataItem
         */
        resetDataItem : function () {
            for (let key in this.dataItem)
            {
                switch (typeof(this.dataItem[key])) {
                    case 'boolean' :
                        this.dataItem[key] = false;
                        break;
                    case 'number' :
                        this.dataItem[key] = 0;
                        break;
                    case 'string' :
                    default:
                        this.dataItem[key] = '';
                }
            }
        },

        /**
         * 加载数据项目 dataItem
         * 需要配置 apiConfig.get
         * @method  loadDataItem
         * @param   {*}     pk      primary key
         */
        loadDataItem : function (pk = null){

            if (this.status === 'loading')
            {
                consoleHelper.logDebug('It\'s loading.');
                return;
            }

            let apiUrl = this.apiConfig.get.url;
            let apiMethod = this.apiConfig.get.method || 'get';
            let dataPath = this.apiConfig.get.dataPath || 'data';
            let idParam = this.apiConfig.get.idParam || 'id';
            let header = this.apiConfig.get.header || null;

            if (!apiUrl)
            {
                consoleHelper.logWarn('Missing get data api url.');
                return;
            }

            let data = this.apiConfig.get.params || {};
            if (pk)     // my data not need id param
            {
                data[idParam] = pk;
            }

            let options = {
                url : apiUrl,
                method : apiMethod,
                data : data
            };
            header ? options.header = header : null;

            this.status = 'loading';
            this.onLoading ? this.onLoading() : null;

            requestManager.request(options, (error, result) => {

                if (error)
                {
                    this.status = 'error';
                    this.onHttpError(error);
                    return;
                }

                // if (result.errorCode !== 0)
                // {
                //     this.status = 'error';
                //     this.onError(result);
                //     return;
                // }

                this.status = 'ready';

                let data        = objectHelper.getDataByKeyPath(result, dataPath);
                data = objectHelper.dataColumnConvert(data, this.apiConfig.get.dataColumnMapping || {});
                this.dataItem = data;

                // prevent trigger request error callback
                try {
                    this.onLoadDataItem(this);
                }
                catch (e)
                {
                    consoleHelper.logError(e);
                }
            });

        },

        /**
         * 重新加载数据项目
         * @description 加载成功后才覆盖原数据
         * @method  reloadDataItem
         * @param   {Number|String}     pk  primary key
         */
        reloadDataItem : function (pk = null){
            this.loadDataItem(pk || this.dataItem.id);
        },

        /**
         * @description use apiConfig.categoryList to load data
         * @method      loadCategoryList
         * @param       {Function}      callback
         * @param       {String|Object} callback.result
         * @param       {String|Object} callback.data
         */
        loadCategoryList : function (callback){

            // API params
            let apiUrl = this.apiConfig.categoryList.url;
            let apiMethod = this.apiConfig.categoryList.method || 'get';
            let dataPath = this.apiConfig.categoryList.dataPath || 'data.rows';
            let params = this.apiConfig.categoryList.params || {};

            // request options
            let options = {
                url : apiUrl,
                method : apiMethod,
                params : params
            };

            // use middleware
            requestManager.request(options, (error, result) => {
                if (error)
                {
                    this.onHttpError(error);
                    return;
                }

                // if (result.errorCode !== 0)
                // {
                //     this.onError(result);
                //     return;
                // }

                let data            = objectHelper.getDataByKeyPath(result.data, dataPath);
                this.categoryList   = data;

                if (callback) { callback(result, data) }
            });
        },

        /**
         * 保存数据项目
         * 需要配置 apiConfig.add 和 apiConfig.update
         * @method  save
         */
        save : function (){

            let apiUrl      = this.dataItem[this.primaryKey] ? this.apiConfig.update.url : this.apiConfig.add.url;
            let apiMethod   = this.dataItem[this.primaryKey] ?
                this.apiConfig.update.method || '' : this.apiConfig.add.method || '';
            let dataPath    = this.dataItem[this.primaryKey] ?
                this.apiConfig.update.dataPath || 'data' : this.apiConfig.add.dataPath || 'data';
            let dataColumnMapping = this.dataItem[this.primaryKey] ?
                this.apiConfig.update.dataColumnMapping || {} :
                this.apiConfig.add.dataColumnMapping;

            let data = objectHelper.clone(this.dataItem);
            // column convert
            data = objectHelper.dataColumnConvertReverse(data, dataColumnMapping);

            if (this.beforeSubmit)
            {
                data = this.beforeSubmit(data);
                // check data, allow prevent submit.
                if (!data)
                {
                    consoleHelper.logError('It must return data in beforeSubmit handler.');
                    return false;
                }
            }

            // use middleware
            requestManager.request({
                url : apiUrl,
                method : apiMethod,
                data : data
            }, (error, res) => {

                if (error)
                {
                    this.onSubmitError ? this.onSubmitError(error) : null;
                    return;
                }

                if (this.onSubmitSuccess)
                {
                    this.onSubmitSuccess(objectHelper.clone(res.data));
                }

                let data = objectHelper.getDataByKeyPath(res.data, dataPath);

                if (!data)
                {
                    return;
                }
                this.dataItem = data;

                this.reset();
                if (this.close || null)
                {
                    this.close();
                }
                else if (this.hide || null)
                {
                    this.hide();
                }

                // if (this.$parent && typeof(this.$parent.reloadData) == 'function')
                // {
                //     this.$parent.reloadDataList();
                // }
            })
        },

        /**
         * 删除数据项目
         * 需要配置 apiConfig.remove
         * @method  removeDataItem
         * @param   {Object}  dataItem
         */
        removeDataItem : function (dataItem){

            let url      = this.apiConfig.remove.url;
            let method   = this.apiConfig.remove.method;
            let header   = this.apiConfig.remove.header || {};
            let dataType    = this.apiConfig.remove.dataType || '';
            let idParam     = this.apiConfig.remove.idParam || 'id';

            let data = {};
            data[idParam] = dataItem ? dataItem[idParam] : this.dataItem[idParam];

            let options = {
                url : url,
                method : method,
                header : header,
                headers : header
            };
            if (method === 'get' || dataType === 'url')
            {
                options.params = data;
            }
            else
            {
                options.data = data;
            }

            requestManager.request(options, (error) => {

                if (error)
                {
                    consoleHelper.logError('remove failed: ' + error.status + ' ' +error.statusText);
                    appHelper.showHint('删除失败', {
                        type : 'error'
                    });
                    return;
                }
                appHelper.showHint('删除成功', {
                    type : 'success'
                });

                this.onRemoveDataItem();
            });
        },

        /*
         * methods for time format
         */

        /**
         * convert timestamp to 'YYYY-MM-DD'
         * @method  formatDate
         * @param   {Number}  timestamp
         * @return {string}
         */
        formatDate : function (timestamp) {
            if (!timestamp)
            {
                return '';
            }
            return moment(timestamp).format('YYYY-MM-DD');
        },

        /**
         * convert timestamp to 'YYYY-MM-DD HH:mm:ss'
         * @method  formatDateTime
         * @param   {Number}  timestamp
         * @return  {string}
         */
        formatDateTime : function (timestamp) {
            if (!timestamp)
            {
                return '';
            }
            return moment(timestamp).format('YYYY-MM-DD HH:mm:ss');
        },

        /**
         * convert timestamp to 'HH:mm:ss'
         * @method  formatDate
         * @param   {Number}  timestamp
         * @return  {string}
         */
        formatTime : function (timestamp) {
            if (!timestamp)
            {
                return '';
            }
            return moment(timestamp).format('HH:mm:ss');
        },

        /**
         * 格式化延迟多久，比如 'n秒前' 'n分钟前' 'n天前'
         * @method  formatDelayTime
         * @param   {Number}  timestamp
         * @return {string}
         */
        formatDelayTime : function (timestamp) {
            if (!timestamp)
            {
                return '';
            }
            return moment(timestamp).fromNow();
        },

        /*
         * methods for internal search controls
         */

        /**
         * fire on search action
         * @event   onSearch
         * @param   {String}    value
         */
        onSearch : function (value){
            if (this.searchInputbox.column === '')
            {
                consoleHelper.logWarn('searchInputbox column is empty.');
                return;
            }

            this.filters[this.searchInputbox.column] = value;
            this.dataList = [];
            this.loadDataList();
        },

        /**
         * @method  getPlainText
         * @param   {String}  html
         * @return  {String}
         */
        getPlainText : function(html) {
            let text = htmlHelper.removeTag(htmlHelper.decode(html));
            return text;
        },

        // event for http request

        /**
         * @event   onHttpError
         * @param   {{Error}}   error
         */
        onHttpError : function (error) {},

        /**
         * @event   onError
         * @param   {{Object}}  result
         * @param   {{Object}}  result.data
         * @param   {{Number}}  result.errorCode
         * @param   {{String}}  result.msg
         */
        onError : function (result) {},

        /**
         * event for data list
         * @event   onLoadDataList
         * @param   {Array}     data
         * @param   {Object}    response data
         */
        onLoadDataList : function () {},

        /**
         * fire before load data, status: loading
         * @event   onLoading
         */
        onLoading : function () {},

        /**
         * event for data item
         * @event   beforeSubmit
         * @description     a handler function for convert data format and return a object to request.
         * @param   {Object}  data
         * @return  {Object}
         */
        beforeSubmit : function (data) {
            return data;
        },

        /**
         * @event   onSubmitSuccess
         * @param   {null|Object}   responseData
         */
        onSubmitSuccess : function () {},

        /**
         * @event   onSubmitError
         * @param   {null|Object}   request error
         */
        onSubmitError : function () {},

        /**
         * @event   onLoadDataItem
         */
        onLoadDataItem : function () {},

        /**
         * @event   onRemoveDataItem
         */
        onRemoveDataItem : function () {},

        /**
         * add a output channel
         * @method  addOutput
         * @param  {String}  id
         * @param  {Object}  control
         */
        addOutput : function (id, control){
            if ( typeof(control) != 'object' )
            {
                consoleHelper.logError('add output error: ' + id + ' is not a object.');
                return;
            }
            this.outputMap[id] = control;
        },

        /**
         * output signal
         * @method  outputSignal
         * @param  {Object}  signal information
         */
        outputSignal : function (signal){
            for (let key in this.outputMap)
            {
                if (typeof(this.outputMap[key].inputSignal) === 'undefined')
                {
                    consoleHelper.log('[warning] inputSignal method not exist in ' + key + ' output control.');
                    continue;
                }
                this.outputMap[key].inputSignal(signal);
            }
        }
    },
    mounted() {

        if (this.enableDataList)        // initialization for data list
        {
            this.pagination.pageSize = this.pageSize;

            // bind event
            this.pagination.showSizeChange  = this.onChangeSize;
            this.pagination.change          = this.onChangePage;

            if (this.autoLoadDataList)
            {
                this.loadDataList();
            }
        }

        // dataItem
        if (this.enableDataItem)
        {
            if (this.autoLoadDataItem)
            {
                let id = appHelper.getRouteParam(this, 'id');
                this.loadDataItem(id);
            }
        }
    }

};
