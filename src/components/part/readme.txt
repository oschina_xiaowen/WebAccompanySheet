这些 Vue 零件是基于预编译的，不能直接在网页上引用，需要使用打包工具 import 后使用。

类继承：
generalCmpPart      通用控件零件，包含一些数据处理功能
-- drawerPart       抽屉弹窗零件，包含一些显示控制方法
-- pagePart         页面零件，包含一些标题方法、加载数据方法
-- modulePart       模块零件，包含一些模块属性

