
import requestManager from '@/components/platformApi/requestManager';

import appHelper    from '../appHelper';

export default {

    verifyLogin(callback) {
        requestManager.request({
            url: 'login/portal/verify_login',
            method: 'post'
        }, (error, result) => {

            if (error)
            {
                appHelper.generalAjaxError(error);
                return;
            }

            callback(result);
        })
    },

    /**
     * @param   {Object}    params
     * @param   {String}    params.loginName
     * @param   {String}    params.password
     * @param   {String}    params.verificationCode
     * @param   {Function}  callback
     */
    login : function(params, callback) {
        requestManager.request({
            url: 'login/portal/login',
            method: 'post',
            data : params
        }, (error, result) => {

            if (error)
            {
                appHelper.generalAjaxError(error);
                return;
            }

            callback(result);
        })
    },

};
