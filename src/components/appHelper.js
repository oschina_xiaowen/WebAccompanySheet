// config
import appConfig from '../appConfig';

// data
import externalApp from '../data/externalApp';

// platformApi
import storageManager   from './platformApi/storageManager';
import requestManager   from './platformApi/requestManager';
import timerManager     from './platformApi/timerManager';

// util
import consoleHelper    from '../util/consoleHelper';
import domHelper        from '../util/domHelper';
import browserHelper    from '../util/browserHelper';
import stringHelper     from '../util/stringHelper';
import urlHelper        from '../util/urlHelper';
import numberHelper     from '../util/numberHelper';
import deviceHelper     from "../util/deviceHelper";

/**
 * @module  util/appHelper
 */
export default {

    /**
     * back to prev page
     * @param   {Null|Object}   controller  page controller
     */
    back: function (controller = null, defaultPath = '/') {

        if (typeof (window) !== 'undefined' && typeof (window.history) !== 'undefined')     // for browser
        {
            consoleHelper.logDebug('window.history.length: ' + window.history.length);
            if (window.history.length > 1) {
                consoleHelper.logDebug('router back');
                controller.$router.back();
            } else {
                consoleHelper.logDebug('router push ' + defaultPath);
                controller.$router.push(defaultPath);
            }
        }
        else if (typeof (uni) !== 'undefined' && typeof(getCurrentPages) !== 'undefined')   // for uniapp framework
        {
            const pages = getCurrentPages();
            if (pages.length > 1) {
                uni.navigateBack()
            } else {
                uni.navigateTo({
                    url : '/pages/index/index'
                });
            }
        }
    },

    /**
     * redirect to page or external website.
     * @param   {String}  url
     * @param   {Function}  callback
     */
    redirect(url, callback) {

        if (typeof (location) === 'object')     // for browser
        {
            // absolute path use platform api to redirect
            consoleHelper.logDebug('redirect to: ' + url);
            if (url.substr(0, 4) === 'http' || url.substr(0, 2) === '//') {
                if (typeof (location) !== 'undefined')   // for browser
                {
                    consoleHelper.log('before redirect to: ' + url);
                    debugger;
                    location.href = url;
                }
                return;
            }

            consoleHelper.log('before redirect to: ' + url);
            debugger;
            window.app.$router.push(url);
        }
        else if (typeof (uni) === 'object')     // for uniapp framework
        {
            uni.navigateTo({
                url : url,
                success : function() {
                    callback ? callback(true) : null;
                },
                fail : function () {
                    callback ? callback(false) : null;
                }
            });
        }
    },

    /**
     * set app page title
     * @param   {String}  title
     */
    setTitle: function (title) {
        if (typeof (document) === 'object')     // for browser
        {
            document.title = title;
        }
        else if (typeof (uni) === 'object')     // for uniapp framework
        {
            uni.setNavigationBarTitle({
                title: title
            });
        }
    },

    /**
     * @return {String}
     */
    getAppIcon: function () {

        if (typeof (document) !== 'undefined')  // for browser
        {
            const iconLinkTag = document.getElementsByName('icon');
            if (!iconLinkTag.length) {
                return '';
            }
            return iconLinkTag[0].href;
        }

        return '';
    },

    /**
     * @return {string}
     */
    getRandomCoverImage: function () {
        let number = numberHelper.getRandomInteger(7) + 1;
        return '//h5.shixun365.com/demo_media/image/article/0' + number + '.jpg';
    },

    /**
     * @return {string}
     */
    getRandomUserHead() {
        let number = numberHelper.getRandomInteger(7) + 1;
        return '//h5.shixun365.com/demo_media/image/user_head/0' + number + '.jpg';
    },

    /**
     * show message, use a fixed dialog, having a ok button.
     * @param   {String}  title
     * @param   {String}  content
     * @param   {Object}  options     not supported currently
     */
    showMessage: function (title, content, options) {

        if (typeof(window) !== 'undefined')   // for browser
        {
            window.app.$refs.messageBox.setData({
                title: title,
                content: content
            });
            window.app.$refs.messageBox.show();
        }
        else if (typeof (uni) === 'object')     // for uniapp framework
        {
            uni.showToast({
                title: title,
                icon: 'none'
            });
        }

    },

    /**
     * show hint, and fade out after 2s.
     * @param   {String}    text
     * @param   {Object}    options
     * @param   {Number}    options.delay
     * @param   {String}    options.type
     * @param   {Function}  callback
     */
    showHint: function (text, options = {}, callback = null) {

        let delay = 3;

        if (typeof(document) !== 'undefined')   // for browser
        {
            delay = options.delay || delay;
            const type  = options.type  || 'info';

            const e = domHelper.createElement('div', {
                'class': 'hintBox alignC ' + type
            }, text);

            document.body.appendChild(e);
            const width     = e.offsetWidth;
            const height    = e.offsetHeight;
            e.style.marginLeft  = '-' + (width / 2) + 'px';
            e.style.marginTop   = '-' + (height / 2) + 'px';
            e.style.opacity = 1;

            window.setTimeout(function () {
                e.parentNode.removeChild(e);
            }, delay * 1000);
        }
        else if (typeof (uni) === 'object')     // for uniapp framework
        {
            uni.showToast({
                title: text,
                icon: 'none'
            });
        }

        callback ? timerManager.setTimeout(callback, delay * 1000) : null;
    },

    /**
     * @param   {Object}  options
     * @param   {String}  options.title
     * @param   {String}  options.content
     * @param   {String}  options.okText
     * @param   {String}  options.cancelText
     * @param   {Function}      callback
     */
    confirm: function (options, callback) {
        if (typeof(document) !== 'undefined')   // for browser
        {
            window.app.$refs.confirmBox.show(options, callback);
        }
    },

    shareAlert: function ()
    {
        let info = '';
        if (browserHelper.isWechat) {
            info = '点击右上角菜单，分享给朋友。';
            this.showMessage(info);
        } else {
            info = '点击浏览器选项菜单，分享给朋友。';
            this.showMessage(info);
        }
    },

    /**
     * @param   {Object}    options
     */
    showLoading: function (options = {})
    {
        if (typeof(document) === 'object')      // for browser
        {
            window.app.$refs.loadingModal.show();
        }
        else if (typeof (uni) === 'object')     // for uniapp framework
        {
            uni.showLoading(options);
        }
    },

    /**
     * @param   {Object}    options
     */
    hideLoading: function (options = {})
    {
        if (typeof(document) !== 'undefined')   // for browser
        {
            window.app.$refs.loadingModal.hide();
        }
        else if (typeof (uni) === 'object')     // for uniapp framework
        {
            uni.hideLoading(options);
        }
    },

    /**
     * @param   {Error} error
     */
    generalAjaxError: function (error) {

        if (!error)     // 在回调执行期间出错（可能是语法等原因）,error 为 null
        {
            this.showHint(appConfig.messageMap.networkError);
            return;
        }
        else if (typeof (error.response) != 'object')    // 没有响应包，可能是客户端网络问题，也可能是服务端没有返回。
        {
            this.showHint(appConfig.messageMap.networkTimeout);
            return;
        }

        const response = error.response;

        // 接下来是真正处理响应包的代码
        const statusCode = response.status;

        const data = response.data;

        if (data.code == 40000)  // 需要登录或超时
        {
            storageManager.removeItem('Authorization');
            if (browserHelper.isWechat) {
                const wechatLoginUrl = error.response.data.data;
                consoleHelper.logDebug('Go to wechat login, url: ' + wechatLoginUrl);
                this.redirect(wechatLoginUrl);
                return;
            }
            consoleHelper.logDebug('Go to login page.');
            this.goToLoginPage();
            return;
        }

        let msg = '加载数据失败';
        if (response) {
            msg += ': ' + statusCode;
            if (typeof (response.data) === 'object') {

                consoleHelper.logError(response.data.msg);

                if (response.data.msg.length < 50)
                {
                    msg += ' ' + response.data.msg + 'errorCode_' + response.data.errorCode;
                }
                else
                {
                    msg += ' <br/>errorCode ' + response.data.errorCode + " <br/>更多信息请查看控制台";
                }

            }
        }

        this.showHint(msg);
    },

    /**
     * @param   {Object}    options
     */
    goToLoginPage: function (options = {}) {

        consoleHelper.logDebug('go to login page.');
        debugger;

        if (typeof (location) !== 'undefined')  // for browser
        {
            storageManager.setItem('pathAfterLogin', window.app.$route.path);
            let url = externalApp.oldShixun.baseUrl + 'html/login/login.html';
            if (window.app.$route.path != '/user/login') {
                // 旧项目用了 gotoBeforeUrl 这个糟糕的名字，不能改
                url = urlHelper.addUrlParam(url, 'gotoBeforeUrl', encodeURIComponent(location.href));
            }
            consoleHelper.log('redirect to: ' + url);
            this.redirect(url);
            return;
        }
        else if (typeof (uni) === 'object')     // for uniapp framework
        {
            consoleHelper.logDebug('go to login page.');
            const urlAfterLogin = options.urlAfterLogin || '';

            // save data to storage
            uni.setStorage({
                key : "urlAfterLogin",
                data : urlAfterLogin
            });

            uni.navigateTo({
                url : '/pages/user/login/index'
            });
        }

    },

    /**
     * @param   {String}    name
     * @param   {Object}    params
     */
    goToPage(name, params = {}) {

        if (typeof (location) === 'object')     // for browser
        {
            window.app.$router.push({
                name : name,
                params : params
            });
        }
    },

    /**
     * @param   {String}  path
     * @param   {Object}  dataMapping
     * @param   {Object}  options
     * @param   {Object}  options.params
     * @example path: /user/:userId  dataMapping: {userId:'10000'}  return /user/10000
     */
    generateShareUrl: function (path, dataMapping = {}, options = {}) {

        const cleanUrl = location.href.split('#')[0];
        const baseUrl = cleanUrl.split('index.html')[0];

        let url = baseUrl + 'index.html';

        let params = options.params || null;
        if (params)
        {
            url = urlHelper.createUrl(url, params);
        }

        path = stringHelper.replaceData(path, dataMapping);
        url += '#' + path;
        return url;
    },

    /**
     * @param   {Function}      callback
     * @param   {Object}        options
     * @param   {Object|null}   options.error
     * @param   {Object|null}   options.user
     */
    checkLogin: function (callback, options = {}) {
        if (typeof (window) !== 'undefined')
        {
            window.app.checkLogin(callback, options);
        }
    },

    /**
     * check login, and go to login page if not logined.
     * @param   {Function}      callback
     * @param   {Object}        options
     * @param   {Object|null}   options.user
     */
    needLogin: function (callback, options = {}) {
        if (typeof (window) !== 'undefined')
        {
            window.app.needLogin(callback, options);
        }
    },

    /**
     * @return   {Boolean}
     */
    isVerbose : function() {
        let verboseParam = urlHelper.getUrlParam('verbose') || '';
        if (verboseParam === 'on') {
            return true;
        }
        return false;
    },

    /**
     * @method  getRouteParam
     * @param   {Object}    controller  page instance
     * @param   {String}    name
     * @param   {String}    defaultValue, default empty string
     * @return  {*}
     */
    getRouteParam(controller, name, defaultValue = '') {
        if (deviceHelper.isBrowser)
        {
            return controller.$route.params[name] || defaultValue;
        }
        else if (typeof(uni) !== 'undefined')
        {
            return controller.$mp.query[name] || defaultValue;
        }
        return defaultValue;
    },

    /**
     * login as mini program
     * @param   {Function}  callback
     * @requires    uni
     */
    login : function (callback) {

        const me = this;
        uni.login({

            success(res) {

                consoleHelper.logDebug('微信登录成功');
                me.showHint('微信登录成功');
                consoleHelper.logDebug(res);

                // login 接口 拿到 code ，可以直接登录
                let code = res.code;
                let accountInfo = uni.getAccountInfoSync();

                requestManager.request({
                    url : appConfig.apiBaseUrl + 'login/wx/applet',
                    method : 'POST',
                    data : {
                        appid : accountInfo.miniProgram.appId,
                        code : code
                    }
                }, (error, result, response) => {

                    if (error)
                    {
                        consoleHelper.logDebug('登录 code 同步到服务端失败');
                        consoleHelper.logDebug(error);
                        return;
                    }

                    let cookie = response.header['Set-Cookie'] || '';

                    storageManager.setItem('cookie', cookie);

                    let errorCode = result.errorCode || null;

                    if (errorCode)
                    {
                        me.showHint(result.msg || '登录失败');
                        return;
                    }

                    me.redirect('/pages/index/index');
                });
            },
            fail(error) {
                consoleHelper.logError('微信登录失败');
                consoleHelper.logError(error);
                me.showHint('微信登录失败');
            }
        });
    },
}

