import Vue from 'vue';
import Router from 'vue-router';
import {
    baseRoutersMap
} from './routerList';

//push 
const VueRouterPush = Router.prototype.push 
Router.prototype.push = function push (to) {
    return VueRouterPush.call(this, to).catch(err => err)
}

//replace
const VueRouterReplace = Router.prototype.replace
Router.prototype.replace = function replace (to) {
  return VueRouterReplace.call(this, to).catch(err => err)
}

Vue.use(Router)

const router = new Router({
    mode: 'hash',
    base: '/',
    routes: baseRoutersMap
})
export default router
