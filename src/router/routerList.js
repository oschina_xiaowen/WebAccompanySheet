export const baseRoutersMap = [
    // 首页版块
    {
        path: '/',
        redirect: '/musicSheet'
    },
    // {
    //     path: '/index',
    //     name: 'index',
    //     component: () => import('@/pages/musicSheet/index.vue')
    // },

    // 乐谱
    {
        path: '/musicSheet',
        name: 'musicSheetIndex',
        component: () => import('@pages/musicSheet/index.vue')
    }, {
        path: '/musicSheet/:fileName',
        name: 'musicSheetDetail',
        component: () => import('@pages/musicSheet/sheet.vue')
    },

    // 其他单页
    // {
    //     path: '/error',
    //     component: () => import('@pages/notFound/error.vue'),
    // },
    {
        path: '/template/subPageList',
        component: () => import('@/pages/subPageListTemplate.vue'),
    }, {
        path: '/template/subPage',
        component: () => import('@/pages/subPageTemplate.vue'),
    }, {
        path: '/template/formPage',
        component: () => import('@/pages/formPageTemplate.vue'),
    }
]
