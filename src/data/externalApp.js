// @var String   production | test | development
let mode = '';

if (process.env.NODE_ENV === 'production') {
    if (process.env.VUE_APP_MODE === 'prod') { // prod 正式环境
        //production 生产环境
        mode = 'prod';
    } else if (process.env.VUE_APP_MODE === 'test') { // test 测试环境
        mode = 'test';
    }
} else if (process.env.NODE_ENV === 'development') { //dev 开发环境
    // development 开发环境
    mode = 'development';
}

export default {

    oldShixun : {
        baseUrl : (function(){
            switch (mode)
            {
                case 'development' :
                case 'test' :
                    // return 'http://localhost:3001/dev/shixun-mobile/';
                    return '//h5.shixun365.com/shixun-mobile/';
                case 'prod' :
                    return '//yj.shixun365.com/shixun-mobile/';
            }
            return '';
        })()
    },

    zysj : {
        baseUrl : (function(){
            switch (mode)
            {
                case 'development' :
                case 'test' :
                    return '//h5.shixun365.com/zysj-mobile/';
                case 'prod' :
                    return '//circle.shixun365.com/zysj-mobile/';
            }
            return '';
        })()
    },

    // 海报小程序
    posterWechatApp : {
        "path" : "/pages/index/index",
        "appId": "gh_6c8280cec067"
    },

    // 课件小程序
    coursewareWechatApp : {
        appId : 'gh_73d6f0dca96b'
    },
    rcWebApp : {
        url : (function(){
            switch (mode)
            {
                case 'development' :
                case 'test' :
                    return 'https://h5.shixun365.com/shixun-rc-mobile/index.html';
                case 'prod' :
                    return 'https://yj.shixun365.com/shixun-rc-mobile/index.html';
            }
            return '';
        })()
    },

    qiDingDong : {
        url : 'https://h.7dingdong.com/#/?store_id=6864356'
    }
};
