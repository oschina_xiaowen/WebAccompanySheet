const path = require('path')

function resolve(dir) {
    return path.join(__dirname, dir)
}

module.exports = {
    chainWebpack: (config) => {
        // 设置全局引用 路径别名
        config.resolve.alias
            .set('@/', resolve('src'))
            .set('@util', resolve('src/util'))
            .set('@router', resolve('src/router'))
            .set('@comp', resolve('src/components'))
            .set('@data', resolve('src/data'))
            .set('@pages', resolve('src/pages'))
    },
    indexPath: 'index.html',
    // publicPath: process.env.NODE_ENV === 'production' ? '/' : '', // 打包时 静态资源引用的 公共路径
    publicPath: 'webaccompanysheet',
    assetsDir: 'static', // 打包出来的静态资源存储目录
    lintOnSave: false,
    filenameHashing: true, // 是否启用 哈希 命名
    runtimeCompiler: true, // 是否使用包含运行时编译器的 Vue 构建版本
    productionSourceMap: true, // 生产环境的 source map
    // ant-ui 主题颜色
    devServer: {
        port: 3001, // 启动端口设置

        // proxy: {
        //     '/api': {
        //        target: 'https://mock.ihx.me/mock/5baf3052f7da7e07e04a5116/antd-pro', //mock API接口系统
        //        ws: false,
        //        changeOrigin: true,
        //        pathRewrite: {
        //          '/jeecg-boot': ''  //默认所有请求都加了jeecg-boot前缀，需要去掉
        //        }
        //      },
        //     '/jeecg-boot': {
        //         target: 'http://localhost:8080', //请求本地 需要jeecg-boot后台项目
        //         ws: false,
        //         changeOrigin: true
        //     },
        // }
    },
    configureWebpack: {
        output: { hashFunction: 'sha512' },
    },
}
